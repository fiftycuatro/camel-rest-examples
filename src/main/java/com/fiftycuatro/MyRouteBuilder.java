package com.fiftycuatro;

import com.fiftycuatro.api.ResponseType;
import com.fiftycuatro.api.ServiceSettings;
import com.fiftycuatro.api.Settings;
import io.swagger.annotations.ApiOperation;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(MyRouteBuilder.class);

    private static final String JSON = "application/json";
    private static final String HTML = "text/html";

    public void configure() {

        // @formatter:off

        restConfiguration()
        .component("jetty")
        .bindingMode(RestBindingMode.auto)
        .enableCORS(true)
        .host("0.0.0.0")
        .port(8081)
        .dataFormatProperty("prettyPrint", "true")
        .corsHeaderProperty("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Access-Control-Allow-Origin");

        // --------------------------------------------------------------------
        // HTML Interface

        from("jetty:http://0.0.0.0:8081/").setHeader("template").constant("index.vm")
          .setHeader("title").constant("Index")
          .to("velocity:com/fiftycuatro/layout.vm");

        from("jetty:http://0.0.0.0:8081/index.html").setHeader("template").constant("index.vm")
                .setHeader("title").constant("Index")
                .to("velocity:com/fiftycuatro/layout.vm");

        from("jetty:http://0.0.0.0:8081/settings.html").setHeader("template").constant("settings.vm")
                .setHeader("title").constant("Settings")
                .to("bean:my-service?method=getServiceSettings")
                .to("velocity:com/fiftycuatro/layout.vm");

        from("jetty:http://0.0.0.0:8081/swagger.html").setHeader("template").constant("swagger.vm")
                .setHeader("title").constant("Swagger")
                .to("velocity:com/fiftycuatro/layout.vm");

        // --------------------------------------------------------------------
        // REST API

        rest("/api/service").id("rest-api").description("Sample Settings")
          .consumes(JSON).produces(JSON)

          .get("/settings").description("Get Settings").outType(ServiceSettings.class)
            .to("bean:my-service?method=getServiceSettings")

          .post("/settings").description("Update Settings").type(ServiceSettings.class)
            .to("bean:my-service?method=setServiceSettings")

          .post("/settings/{id}").description("Settings").type(Settings.class).outType(ResponseType.class)
            .param().name("id").description("ID of setting").endParam()
            .route()
            .to("bean:my-service?method=addOrUpdateSetting(${headers.id}, ${body})")
            .setBody().constant(new ResponseType(true, "success"))
            .transform().body().endRest();
            //.to("bean:my-service?method=getServiceSettings")
            //.to("file:./settings?fileName=my-service-settings.json");

        // --------------------------------------------------------------
        // Static Content
        from("jetty:http://0.0.0.0:8081?handlers=#static-handler").to("log:static");


        // @formatter:on
    }

}
