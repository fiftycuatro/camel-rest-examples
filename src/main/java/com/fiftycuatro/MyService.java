package com.fiftycuatro;

import com.fiftycuatro.api.ServiceSettings;
import com.fiftycuatro.api.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyService {

    private static final Logger LOG = LoggerFactory.getLogger(MyService.class);

    private ServiceSettings settings;

    public MyService() {
        this.settings = new ServiceSettings();

        Settings defaultSetting = new Settings();
        defaultSetting.setId("DEFAULT");
        defaultSetting.setOption1(100);
        defaultSetting.setOption2(2012.2);

        Settings settings1 = new Settings();
        settings1.setId("Settings1");
        settings1.setOption1(100);
        settings1.setOption2(2012.2);

        Settings settings2 = new Settings();
        settings2.setId("Settings2");
        settings2.setOption1(5100);
        settings2.setOption2(12312.2);

        this.settings.setDefaultSettings(defaultSetting);
        this.settings.getSomeSettings().put("Settings1", settings1);
        this.settings.getSomeSettings().put("Settings2", settings2);
    }

    public void setServiceSettings(ServiceSettings settings) {
        this.settings = settings;
    }

    public ServiceSettings getServiceSettings() {
        return this.settings;
    }

    public void addOrUpdateSetting(String currentId, Settings settings) {
        ServiceSettings updated = this.settings.clone();

        if (settings.getId().equals("DEFAULT")) {
            updated.setDefaultSettings(settings);
        } else {
            if (!currentId.equals(settings.getId())) {
                updated.getSomeSettings().remove(currentId);
            }
            updated.getSomeSettings().put(settings.getId(), settings);
        }
        setServiceSettings(updated);
    }
}
