package com.fiftycuatro;

import io.swagger.jaxrs.config.BeanConfig;
import org.apache.camel.impl.DefaultClassResolver;
import org.apache.camel.spi.ClassResolver;
import org.apache.camel.spi.RestConfiguration;
import org.apache.camel.swagger.RestSwaggerSupport;
import org.apache.camel.swagger.SwaggerHelper;
import org.apache.camel.swagger.servlet.RestSwaggerServlet;
import org.apache.camel.swagger.servlet.ServletRestApiResponseAdapter;
import org.apache.camel.util.EndpointHelper;
import org.apache.camel.util.ObjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

// Unfortunate duplicate of RestSwaggerServlet with no initialization of path and host
public class MyRestSwaggerServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(MyRestSwaggerServlet.class);
    private final BeanConfig swaggerConfig = new BeanConfig();
    private final RestSwaggerSupport support = new RestSwaggerSupport();
    private final ClassResolver classResolver = new DefaultClassResolver();
    private volatile boolean initDone;
    private String apiContextIdPattern;
    private boolean apiContextIdListing;

    public MyRestSwaggerServlet() {
    }

    public String getApiContextIdPattern() {
        return this.apiContextIdPattern;
    }

    public void setApiContextIdPattern(String apiContextIdPattern) {
        this.apiContextIdPattern = apiContextIdPattern;
    }

    public boolean isApiContextIdListing() {
        return this.apiContextIdListing;
    }

    public void setApiContextIdListing(boolean apiContextIdListing) {
        this.apiContextIdListing = apiContextIdListing;
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        HashMap parameters = new HashMap();
        Enumeration en = config.getInitParameterNames();

        while(en.hasMoreElements()) {
            String pattern = (String)en.nextElement();
            String listing = config.getInitParameter(pattern);
            parameters.put(pattern, listing);
        }

        if(parameters.get("cors") != null) {
            LOG.warn("Use RestSwaggerCorsFilter when uisng this Servlet to enable CORS");
            parameters.remove("cors");
        }

        this.support.initSwagger(this.swaggerConfig, parameters);
        Object pattern1 = parameters.remove("apiContextIdPattern");
        if(pattern1 != null) {
            this.apiContextIdPattern = pattern1.toString();
        }

        Object listing1 = parameters.remove("apiContextIdListing");
        if(listing1 != null) {
            this.apiContextIdListing = Boolean.valueOf(listing1.toString()).booleanValue();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Object contextId = null;
        String route = request.getPathInfo();
        String accept = request.getHeader("CamelAcceptContentType");
        boolean json = false;
        boolean yaml = false;
        if(route != null && route.endsWith("/swagger.json")) {
            json = true;
            route = route.substring(0, route.length() - 13);
        } else if(route != null && route.endsWith("/swagger.yaml")) {
            yaml = true;
            route = route.substring(0, route.length() - 13);
        }

        if(accept != null && !json && !yaml) {
            json = accept.contains("json");
            yaml = accept.contains("yaml");
        }

        if(!json && !yaml) {
            json = true;
        }

        ServletRestApiResponseAdapter adapter = new ServletRestApiResponseAdapter(response);

        try {
            if(!this.apiContextIdListing || !ObjectHelper.isEmpty(route) && !route.equals("/")) {
                String e = null;
                if(ObjectHelper.isNotEmpty(route)) {
                    if(route.startsWith("/")) {
                        route = route.substring(1);
                    }

                    e = route.split("/")[0];
                    if(ObjectHelper.isNotEmpty(e)) {
                        route = route.substring(e.length());
                    }
                } else {
                    List match = this.support.findCamelContexts();
                    if(match.size() == 1) {
                        e = (String)match.get(0);
                    }
                }

                boolean match1 = false;
                if(e != null) {
                    match1 = true;
                    if(this.apiContextIdPattern != null) {
                        if("#name#".equals(this.apiContextIdPattern)) {
                            match1 = true;
                        } else {
                            match1 = EndpointHelper.matchPattern(e, this.apiContextIdPattern);
                        }

                        if(LOG.isDebugEnabled()) {
                            LOG.debug("Match contextId: {} with pattern: {} -> {}", new Object[]{e, this.apiContextIdPattern, Boolean.valueOf(match1)});
                        }
                    }
                }

                if(!match1) {
                    adapter.noContent();
                } else {
                    this.support.renderResourceListing(adapter, this.swaggerConfig, e, route, json, yaml, this.classResolver, (RestConfiguration)null);
                }
            } else {
                this.support.renderCamelContexts(adapter, (String)contextId, this.apiContextIdPattern, json, yaml, (RestConfiguration)null);
            }
        } catch (Exception var11) {
            LOG.warn("Error rendering Swagger API due " + var11.getMessage(), var11);
        }

    }

    private String translateContextPath(HttpServletRequest request) {
        String path = request.getContextPath();
        if(!path.isEmpty() && !path.equals("/")) {
            int idx = path.lastIndexOf("/");
            return idx > 0?path.substring(0, idx):path;
        } else {
            return "";
        }
    }
}
