package com.fiftycuatro.api;

public class ResponseType {

    private boolean successful;
    private String message;

    public ResponseType() {

    }

    public ResponseType(boolean isSuccessful, String message) {
        this.successful = isSuccessful;
        this.message = message;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
