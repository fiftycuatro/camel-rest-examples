package com.fiftycuatro.api;

import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@ApiModel(description = "Basic settings for service")
public class ServiceSettings implements Cloneable {

    private Settings defaultSettings;
    private Map<String, Settings> someSettings;

    public ServiceSettings() {
        this.someSettings = new HashMap<>();
    }

    public Settings getDefaultSettings() {
        return defaultSettings;
    }

    public void setDefaultSettings(Settings defaultSettings) {
        this.defaultSettings = defaultSettings;
    }

    public Map<String, Settings> getSomeSettings() {
        return someSettings;
    }

    public void setSomeSettings(Map<String, Settings> someSettings) {
        this.someSettings = someSettings;
    }

    @Override
    public ServiceSettings clone() {
        ServiceSettings clone = new ServiceSettings();
        clone.setDefaultSettings(defaultSettings.clone());
        Map<String, Settings> cloneSettings = new HashMap<>();
        for(Settings settings : someSettings.values()) {
            cloneSettings.put(settings.getId(), settings.clone());
        }
        clone.setSomeSettings(cloneSettings);
        return clone;
    }
}
