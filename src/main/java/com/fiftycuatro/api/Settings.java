package com.fiftycuatro.api;

public class Settings implements Cloneable {
    private String id;
    private int option1;
    private double option2;

    public Settings() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOption1() {
        return option1;
    }

    public void setOption1(int option1) {
        this.option1 = option1;
    }

    public double getOption2() {
        return option2;
    }

    public void setOption2(double option2) {
        this.option2 = option2;
    }

    @Override
    public Settings clone() {
        Settings clone = new Settings();
        clone.setId(id);
        clone.setOption1(option1);
        clone.setOption2(option2);
        return clone;
    }
}
