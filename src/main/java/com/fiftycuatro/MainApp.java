package com.fiftycuatro;

import org.apache.camel.CamelContext;
import org.apache.camel.main.Main;
import org.apache.camel.swagger.servlet.RestSwaggerCorsFilter;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.*;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;

import javax.servlet.DispatcherType;
import java.net.InetSocketAddress;
import java.util.EnumSet;

public class MainApp {

    public static void main(String... args) throws Exception {

        ResourceHandler resHandler = new ResourceHandler();
        resHandler.setBaseResource(Resource.newClassPathResource("public"));

        Main main = new Main();
        main.enableHangupSupport();
        main.bind("my-service", new MyService());
        main.bind("static-handler", resHandler);
        main.addRouteBuilder(new MyRouteBuilder());

        startSwaggerServer(main.getOrCreateCamelContext());

        main.run(args);
    }

    private static Server startSwaggerServer(CamelContext camelContext) throws Exception {
        Server server = new Server(new InetSocketAddress("0.0.0.0", 9090));
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        context.setContextPath("/");
        context.setAttribute("CamelContext", camelContext);

        ResourceHandler resHandler = new ResourceHandler();
        resHandler.setBaseResource(Resource.newClassPathResource("/public/swagger-ui-3"));

        HandlerCollection handlers = new HandlerCollection();
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] { context });
        RequestLogHandler requestLogHandler = new RequestLogHandler();
        handlers.setHandlers(new Handler[]{resHandler, contexts, new DefaultHandler(), requestLogHandler});
        server.setHandler(handlers);

        NCSARequestLog requestLog = new NCSARequestLog("./jetty-yyyy_mm_dd.request.log");
        requestLog.setRetainDays(90);
        requestLog.setAppend(true);
        requestLog.setExtended(false);
        requestLog.setLogTimeZone("GMT");
        requestLogHandler.setRequestLog(requestLog);

        ServletHolder swaggerServlet = new ServletHolder(new MyRestSwaggerServlet());
        swaggerServlet.setInitParameter("base.path", "/");
        swaggerServlet.setInitParameter("host", "localhost:8081");
        swaggerServlet.setInitParameter("api.path", "http://localhost:9090/api-docs");
        swaggerServlet.setInitParameter("api.version", "0.0.1");
        swaggerServlet.setInitParameter("api.title", "Example Services API");

        context.addServlet(swaggerServlet, "/api-docs/*");
        context.addFilter(new FilterHolder(new RestSwaggerCorsFilter()), "/api-docs/*", EnumSet.of(DispatcherType.REQUEST));

        server.start();
        return server;
    }
}

